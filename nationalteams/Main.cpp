#include "plugin-std.h"
#include "eCountryId.h"
#include <map>

using namespace plugin;

unsigned char southAmericaNTs[] = { COUNTRY_BRAZIL, COUNTRY_ARGENTINA, COUNTRY_COLOMBIA, COUNTRY_URUGUAY, COUNTRY_CHILE,
COUNTRY_BOLIVIA, COUNTRY_ECUADOR, COUNTRY_PARAGUAY, COUNTRY_PERU, COUNTRY_VENEZUELA };

unsigned char northAmericaNTs[] = { COUNTRY_MEXICO, COUNTRY_UNITED_STATES, COUNTRY_CANADA, COUNTRY_JAMAICA, COUNTRY_COSTA_RICA,
COUNTRY_ANTIGUA_AND_BARBUDA, COUNTRY_ARUBA, COUNTRY_BAHAMAS, COUNTRY_BARBADOS,
COUNTRY_BELIZE,  COUNTRY_BERMUDA, COUNTRY_BRITISH_VIRGIN_IS, COUNTRY_CAYMAN_ISLANDS,
COUNTRY_CUBA, COUNTRY_DOMINICA, COUNTRY_DOMINICAN_REPUBLIC, COUNTRY_EL_SALVADOR, COUNTRY_GRENADA,
COUNTRY_GUATEMALA, COUNTRY_GUYANA, COUNTRY_HAITI, COUNTRY_HONDURAS, COUNTRY_MONTSERRAT,
COUNTRY_NETHERLANDS_ANTIL, COUNTRY_NICARAGUA, COUNTRY_PANAMA, COUNTRY_PUERTO_RICO,
COUNTRY_ST_KITTS_AND_NEVIS, COUNTRY_ST_LUCIA, COUNTRY_ST_VINCENT_AND_GREN, COUNTRY_SURINAM,
COUNTRY_TRINIDAD_AND_TOBAGO, COUNTRY_TURKS_AND_CAICOS, COUNTRY_US_VIRGIN_ISLANDS };

unsigned char africanNTs[] = { COUNTRY_EGYPT, COUNTRY_DR_CONGO, COUNTRY_GHANA, COUNTRY_CAMEROON, COUNTRY_MALI, COUNTRY_MOROCCO,
COUNTRY_SENEGAL, COUNTRY_SOUTH_AFRICA, COUNTRY_ZAMBIA, COUNTRY_ALGERIA, COUNTRY_ANGOLA, COUNTRY_BENIN,
COUNTRY_BOTSWANA, COUNTRY_BURKINA_FASO, COUNTRY_BURUNDI, COUNTRY_CAPE_VERDE_ISLANDS,
COUNTRY_CENTRAL_AFRICAN_REP, COUNTRY_CHAD, COUNTRY_CONGO, COUNTRY_COTE_D_IVOIRE, COUNTRY_DJIBOUTI,
COUNTRY_EQUATORIAL_GUINEA, COUNTRY_ERITREA, COUNTRY_ETHIOPIA, COUNTRY_GABON, COUNTRY_GAMBIA, COUNTRY_GUINEA,
COUNTRY_GUINEA_BISSAU, COUNTRY_KENYA, COUNTRY_LESOTHO, COUNTRY_LIBERIA, COUNTRY_LIBYA, COUNTRY_MADAGASCAR,
COUNTRY_MALAWI, COUNTRY_MAURITANIA, COUNTRY_MAURITIUS, COUNTRY_MOZAMBIQUE, COUNTRY_NAMIBIA, COUNTRY_NIGER,
COUNTRY_NIGERIA, COUNTRY_RWANDA, COUNTRY_SAO_TOME_E_PRINCIPE, COUNTRY_SEYCHELLES, COUNTRY_SIERRA_LEONE,
COUNTRY_SOMALIA, COUNTRY_SUDAN, COUNTRY_SWAZILAND, COUNTRY_TANZANIA, COUNTRY_TOGO, COUNTRY_TUNISIA,
COUNTRY_UGANDA, COUNTRY_ZIMBABWE };

unsigned char asiaNTs[] = { COUNTRY_CHINA_PR, COUNTRY_JAPAN, COUNTRY_IRAN, COUNTRY_SAUDI_ARABIA, COUNTRY_UNITED_ARAB_EMIRATES,
COUNTRY_AUSTRALIA, COUNTRY_AFGHANISTAN, COUNTRY_BAHRAIN, COUNTRY_BANGLADESH, COUNTRY_BHUTAN,
COUNTRY_BRUNEI_DARUSSALAM, COUNTRY_CAMBODIA, COUNTRY_TAIWAN, COUNTRY_GUAM, COUNTRY_HONG_KONG, COUNTRY_INDIA,
COUNTRY_INDONESIA, COUNTRY_IRAQ, COUNTRY_JORDAN, COUNTRY_KAZAKHSTAN, COUNTRY_KOREA_DPR, COUNTRY_KOREA_REPUBLIC,
COUNTRY_KUWAIT, COUNTRY_KYRGYZSTAN, COUNTRY_LAOS, COUNTRY_LEBANON, COUNTRY_MACAO, COUNTRY_MALAYSIA,
COUNTRY_MALDIVES, COUNTRY_MONGOLIA, COUNTRY_MYANMAR, COUNTRY_NEPAL, COUNTRY_OMAN, COUNTRY_PAKISTAN,
COUNTRY_PALESTINIAN_AUTHORITY, COUNTRY_PHILIPPINES, COUNTRY_QATAR, COUNTRY_SINGAPORE, COUNTRY_SRI_LANKA,
COUNTRY_SYRIA, COUNTRY_TAJIKISTAN, COUNTRY_THAILAND, COUNTRY_TURKMENISTAN, COUNTRY_UZBEKISTAN, COUNTRY_VIETNAM,
COUNTRY_YEMEN };

unsigned char oceaniaNTs[] = { COUNTRY_NEW_ZEALAND, COUNTRY_ANGUILLA, COUNTRY_COOK_ISLANDS, COUNTRY_FIJI, COUNTRY_VANUATU,
COUNTRY_AMERICAN_SAMOA, COUNTRY_PAPUA_NEW_GUINEA, COUNTRY_SAMOA, COUNTRY_SOLOMON_ISLANDS, COUNTRY_TAHITI, COUNTRY_TONGA };

class NationalTeams {
public:
    static unsigned int GetNumCountriesForContinent(unsigned int continent) {
        switch (continent)
        {
        case 1:
            return sizeof(southAmericaNTs);
        case 2:
            return sizeof(northAmericaNTs);
        case 3:
            return sizeof(africanNTs);
        case 4:
            return sizeof(asiaNTs);
        case 5:
            return sizeof(oceaniaNTs);
        }
        return 0;
    }

    template<unsigned int N>
    static int METHOD GetCountriesCount(void *t) {
        return N;
    }

    static unsigned int * METHOD GetNationalTeamID(void *t, DUMMY_ARG, unsigned int index, unsigned int *out) {
        *out = (index + 1) | 0xFFFF0000;
        return out;
    }

    static char const *METHOD OnGetCompetitionName(void *, DUMMY_ARG, int) {
        return "";
    }

    NationalTeams() {
        auto v = FM::GetAppVersion();
        if (v.id() == VERSION_FM_13) {
            for (int i = 0x24A3774; i < 0x24A3774 + 36; i++)
                patch::SetChar(i, 0);
            for (int i = 0x24CFBF4; i < 0x24CFBF4 + 10; i++)
                patch::SetChar(i, 0);
            // 1 - south america
            patch::SetPointer(0x117752C + 1, southAmericaNTs);
            patch::SetPointer(0x1177665 + 2, southAmericaNTs);
            patch::SetUInt(0x1177531 + 1, sizeof(southAmericaNTs));
            // 2 - north america
            patch::SetPointer(0x1177538 + 1, northAmericaNTs);
            patch::SetPointer(0x117766C + 2, northAmericaNTs);
            patch::SetUInt(0x117753D + 1, sizeof(northAmericaNTs));
            // 3 - africa
            patch::SetPointer(0x1177544 + 1, africanNTs);
            patch::SetPointer(0x1177673 + 2, africanNTs);
            patch::SetUInt(0x1177549 + 1, sizeof(africanNTs));
            // 4 - asia
            patch::SetPointer(0x1177550 + 1, asiaNTs);
            patch::SetPointer(0x117767A + 2, asiaNTs);
            patch::SetUInt(0x1177555 + 1, sizeof(asiaNTs));
            // 5 - oceania
            patch::SetPointer(0x117755C + 1, oceaniaNTs);
            patch::SetPointer(0x1177681 + 2, oceaniaNTs);
            patch::SetUInt(0x1177561 + 1, sizeof(oceaniaNTs));

            patch::RedirectJump(0x11775F0, GetNumCountriesForContinent);
        }
        else if (v.id() == ID_FM_05_1010_C) {
            patch::RedirectJump(0x73F254, (void *)0x73F27F); // fake players in NT
            for (int i = 0xA51428; i < 0xA51428 + 3; i++) // unable to select NT - Europe
                patch::SetChar(i, 0);
            patch::SetUChar(0x4CB422 + 2, 0); // NT level
            patch::SetUShort(0x6A1BB9 + 2, 0); // NT level

            // 1 - south america
            patch::SetPointer(0x5B0460 + 3, southAmericaNTs);
            patch::SetUChar(0x5B0470 + 2, sizeof(southAmericaNTs));
            // 2 - north america
            patch::SetPointer(0x5B0445 + 3, northAmericaNTs);
            patch::SetUChar(0x5B0451 + 2, sizeof(northAmericaNTs));
            // 3 - africa
            patch::SetPointer(0x5B0430 + 3, africanNTs);
            patch::SetUChar(0x5B043C + 2, sizeof(africanNTs));
            // 4 - asia
            patch::SetPointer(0x5B0400 + 3, asiaNTs);
            patch::SetUChar(0x5B040C + 2, sizeof(asiaNTs));
            // 5 - oceania
            patch::SetPointer(0x5B0415 + 3, oceaniaNTs);
            patch::SetUChar(0x5B0421 + 2, sizeof(oceaniaNTs));

            patch::Nop(0x4CB39B, 6); // skip competition check
            patch::RedirectCall(0x4CB3A3, GetCountriesCount<206>); // get teams count 1
            patch::RedirectCall(0x4CB4E4, GetCountriesCount<206>); // get teams count 2
            patch::RedirectCall(0x4CB3C3, GetNationalTeamID); // get team id
            patch::SetUInt(0x4CB54B + 1, 0x9B5334);
            patch::RedirectCall(0x4CB519, OnGetCompetitionName);

            patch::SetUChar(0x4CB2F9, 0xEB); // remove host teams from the list
            patch::SetUChar(0x4CB337, 0xEB); // remove host teams from the list
        }
    }
} nationalTeams;
